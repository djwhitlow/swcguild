function checkInput() {
	var custName = document.getElementById("custName");
	var phone = document.getElementById("phone");
	var email = document.getElementById("email");
	var reason = document.getElementById("reason");
	var info = document.getElementById("addInfo");
	
	if (!custName.value) alert("Please fill in your name.");
	else if (!email.value && !phone.value) alert("Please leave a phone number or email address");
	else if (reason.value == "other" && !addInfo.value) alert("Please leave us a message.");
	else if (!daySelected()) alert("Please let us know when to contact you.");
}

function daySelected() {
		for (var i = 0; i < 5; i++) {
		if (document.getElementsByName("day")[i].checked) return(true);
	}
}