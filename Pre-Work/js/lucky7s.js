var cash;
var maxCash;
var maxRolls =0;
var roll = 0;
var plays = 0;
var dice1, dice2, total;


function startGame() {
	roll = 0;
	startCash = parseInt(document.getElementById("bet").value);
	cash = maxCash = startCash;
	if (cash < 1) {
		alert("Minimum bet is $1");
		return;
	}
	rolling = window.setInterval(rollDice, 300);
	document.getElementById("results").style.display = "none";
	document.getElementById("stats").style.display = "block";
}

function rollDice() {
	roll++;
	dice1 = Math.floor((Math.random() * 6)+1);
	dice2 = Math.floor((Math.random() * 6)+1);
	total = dice1 + dice2;
	if (total == 7) {
		cash = cash + 4;
		document.getElementById("rollTotal").style.backgroundColor = "green";
	} else {
		--cash;
		document.getElementById("rollTotal").style.backgroundColor = "red";
	}
	if (cash > maxCash) {
		maxCash = cash;
		maxRolls = roll;
	}
	document.getElementById("die1").textContent = convertToDie(dice1);
	document.getElementById("die2").textContent = convertToDie(dice2);
	document.getElementById("rollTotal").textContent = total;
	document.getElementById("cash").textContent = cash;
	document.getElementById("roll").textContent = roll;
	if (cash < 1) {
		clearInterval(rolling);
		endGame();
	}
}

function endGame() {
	plays++;
	document.getElementById("play").value = "Play Again";
	document.getElementById("stats").style.display = "none";
	document.getElementById("results").style.display = "block";
	document.getElementById("startBet").textContent = startCash;
	document.getElementById("brokeIn").textContent = roll;
	document.getElementById("maxWin").textContent = maxCash;
	document.getElementById("maxRoll").textContent = maxRolls;
	if (plays > 4) {
		alert("Your life doesn't have to be like this.")
		window.location.href = "http://www.gamblersanonymous.org/ga/";
	}
}

function convertToDie(number) {
	switch(number) {
		case 1:
			return("⚀");
		case 2:
			return("⚁");
		case 3:
			return("⚂");
		case 4:
			return("⚃");
		case 5:
			return("⚄");
		case 6:
			return("⚅");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}